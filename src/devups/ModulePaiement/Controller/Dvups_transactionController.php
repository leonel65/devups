<?php


use DClass\devups\Datatable as Datatable;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
class Dvups_transactionController extends Controller{

    public function listView($next = 1, $per_page = 10){

        $lazyloading = $this->lazyloading(new Dvups_transaction(), $next, $per_page);
        self::$jsfiles[] = node_modules.'pdfmake/build/pdfmake.min.js';
        self::$jsfiles[] = node_modules.'pdfmake/build/vfs_fonts.js';
        self::$jsfiles[] = Dvups_transaction::classpath('Ressource/js/transactionCtrl.js');

        $this->entitytarget = 'Devups_transaction';
        $this->title = "Manage Dvups_transaction";

        $this->renderListView(Dvups_transactionTable::init($lazyloading)->buildindextable()->render());

    }

    public function datatable($next, $per_page) {
        $lazyloading = $this->lazyloading(new Dvups_transaction(), $next, $per_page);
        return ['success' => true,
            'datatable' => Dvups_transactionTable::init($lazyloading)->buildindextable()->getTableRest(),
        ];
    }

    public function createAction($transaction_form = null){
        extract($_POST);

        $transaction = $this->form_fillingentity(new Dvups_transaction(), $transaction_form);


        if ( $this->error ) {
            return 	array(	'success' => false,
                            'transaction' => $transaction,
                            'action' => 'create',
                            'error' => $this->error);
        }

        $id = $transaction->__insert();
        return 	array(	'success' => true,
                        'transaction' => $transaction,
                        'tablerow' => Dvups_transactionTable::init()->buildindextable()->getSingleRowRest($transaction),
                        'detail' => '');

    }

    public function updateAction($id, $transaction_form = null){
        extract($_POST);

        $transaction = $this->form_fillingentity(new Dvups_transaction($id), $transaction_form);


        if ( $this->error ) {
            return 	array(	'success' => false,
                            'transaction' => $transaction,
                            'action_form' => 'update&id='.$id,
                            'error' => $this->error);
        }

        $transaction->__update();
        return 	array(	'success' => true,
                        'transaction' => $transaction,
                        'tablerow' => Dvups_transactionTable::init()->buildindextable()->getSingleRowRest($transaction),
                        'detail' => '');

    }


    public function detailView($id)
    {

        $this->entitytarget = 'Devups_transaction';
        $this->title = "Detail Transaction";

        $transaction = Dvups_transaction::find($id);

        $this->renderDetailView(
            Dvups_transactionTable::init()
                ->builddetailtable()
                ->renderentitydata($transaction)
        );

    }

    public function deleteAction($id){

            Dvups_transaction::delete($id);
        return 	array(	'success' => true,
                        'detail' => '');
    }


    public function deletegroupAction($ids)
    {

        Dvups_transaction::delete()->where("id")->in($ids)->exec();

        return array('success' => true,
                'detail' => '');

    }
    public function paiement(){
        $trans = new Dvups_transaction();
        $trans->setMontant(Request::post('montant'));
        $trans->setHeure(Request::post('heure'));
        $trans->setDvups_agregateur(Dvups_agregateur::select()->where('nom',Request::post('agregateur'))->__getOne());
        $trans->__insert();
        Response::set('agregateur',Request::post('agregateur'));
        Response::set('montant',Request::post('montant'));
        Response::set('heure',Request::post('heure'));
        return Response::$data;

    }

    public function listeTransaction(){
        return[
            'success'=>true,
            'transaction' =>Dvups_transaction::select()->__getAll()
        ];
    }

    public  function getdataAction(){

    $transaction = Dvups_transaction::all();
    return[
        'transaction'=>$transaction
    ];

    }

    public function facture(){
        try {
            ob_start();
            ?>
            <style type="text/css">
                <!--
                table { vertical-align: top; }
                tr    { vertical-align: top; }
                td    { vertical-align: top; }
                -->
            </style>

            <page backcolor="#FEFEFE"   backimgx="center" backimgy="bottom" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 12pt">
                <bookmark title="Lettre" level="0" ></bookmark>
                      <table cellspacing="0" style="width: 100%; border: solid 2px #721c24; background: #721c24;">
                    <tr>
                        <td style="width: 155%; color: #444444;">
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px;margin-bottom: 130px;margin-top:5%">
                    <tr>
                        <td style="width: 155%;text-decoration: underline">
                            BILAN DES VENTES JOURNALIERES
                        </td>
                    </tr>
                </table>
                <div>
                    PERIODE:<br>
                </div>
                <table cellspacing="0" style="width: 100%; border: solid 1px white; font-size: 10pt;margin-top: 100px">
                    <colgroup>
                        <col style="width: 25%; text-align: center">
                        <col style="width: 25%; text-align: center">
                        <col style="width: 20%; text-align: center">
                        <col style="width: 30%; text-align: center">
                    </colgroup>
                    <thead>
                    <tr style="background: #721c24;color:white;">
                        <th style="border-bottom: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;height:2%;padding-top:30px">Date / heure</th>
                        <th style="border-bottom: solid 1px black;border-right: solid 1px black;height:2%;padding-top:30px">Montant</th>
                        <th style="border-bottom: solid 1px black;border-right: solid 1px black;height:2%;padding-top:30px">taux reduction (%)</th>
                        <th style="border-bottom: solid 1px black;border-right: solid 1px black;height:2%;padding-top:30px">Montant Total</th>
                    </tr>
                    </thead>
                    <tbody>
                <?php
                $transaction=Dvups_transaction::all();
                $all_transaction=Dvups_transaction::select("sum(this.montant) as total")->exec(DBAL::$FETCH);
                foreach ($transaction as $a){?>
                    <tr>
                        <th style="height: 5px;border-left: 1px solid black;border-bottom: 1px solid #1d2124;padding-top: 15px"><?=$a->getHeure()?></th>
                        <th style="height: 5px;border-bottom: 1px solid black;border-left: 1px solid #1d2124;padding-top: 15px"><?=$a->montant?></th>
                        <th style="height: 5px;border-bottom: 1px solid black;border-left: 1px solid #1d2124;padding-top: 15px">%1</th>
                        <th style="height: 5px;border-bottom: 1px solid black;border-right: 1px solid black;border-left: 1px solid #1d2124;padding-top: 15px">
                            <?php
                            $montant=$a->montant;
                            echo $montant-$montant*0.01?>
                        </th>
                    </tr>
               <?php }
                ?>
                    <tr>
                        <th colspan="3" style="text-align: center;border-right: 1px solid white"></th>
                        <th style="height:15px;font-size:20px;padding-top:25px;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black">
                            <?php
                            $total=$all_transaction['total'];
                            $total=$total-0.1*$total;
                            echo($total.' FCFA');
                            ?>
                        </th>
                    </tr>
                    </tbody>
                </table>
                <br>
            </page>
            <?php
            $content = ob_get_clean();

            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content);
            $html2pdf->output('facture.pdf');
        } catch (Html2PdfException $e) {
            $html2pdf->clean();

            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }

}
