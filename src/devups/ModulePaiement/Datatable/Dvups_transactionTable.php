<?php 


use DClass\devups\Datatable as Datatable;

class Dvups_transactionTable extends Datatable{
    
    public $entity = "dvups_transaction";

    public $datatablemodel = [
['header' => 'Heure', 'value' => 'heure'], 
['header' => 'Montant', 'value' => 'montant']
    ];

    public function __construct($lazyloading = null, $datatablemodel = [])
    {
        parent::__construct($lazyloading, $datatablemodel);
    }

    public static function init($lazyloading = null){
        $dt = new Dvups_transactionTable($lazyloading);
        return $dt;
    }

    public function buildindextable(){
        $this->addgroupaction("printpdf");
        $this->enabletopaction=false;
        $this->enablecolumnaction=false;
        return $this;
    }
    
    public function builddetailtable()
    {
        $this->datatablemodel = [
            ['label' => 'Heure', 'value' => 'heure'],
            ['label' => 'Montant', 'value' => 'montant'],
            ['label' => 'Dvups_agregateur', 'value' => 'Dvups_agregateur.nom']
];
        return $this;
    }

}
