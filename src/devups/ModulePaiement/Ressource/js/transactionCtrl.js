function printpdf(el){
    model._get("transaction.getdata", function(response){
        console.log(response);
        var pdfbody = [
            [{text:'Date',bold:true,fontSize:13},{text:'Montant',bold:true,fontSize:13}],
        ];
        var transaction= response.transaction;

        transaction.forEach(function(transaction){
            pdfbody.push([transaction.date,transaction.montant])
        });
        var docDefinition = {
            content: [
                {
                    text:'Liste des transaction',
                    alignment:'center',
                    fontSize:22,
                    color:'indigo'

                },

                {
                    table :{
                        body: pdfbody
                    }
                }
            ]
        };
        pdfMake.createPdf(docDefinition).open();

    });


}